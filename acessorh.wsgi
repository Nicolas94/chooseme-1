import os, sys
sys.path.append('/home/ubuntu/public_html/chooseme')

os.environ['DJANGO_SETTINGS_MODULE'] = 'chooseme.settings'
 
import django.core.handlers.wsgi
 
application = django.core.handlers.wsgi.WSGIHandler()
